# HMEnergyCalculator

An Android app for the game Harry Potter: Hogwarts Mystery by JamCity and Portkey Games, which calculates how much time remains till the energy is full.

(This app is unofficial and is not affiliated with JamCity and Portkey Games)

## Usage: 

Run the app and go to the game. When the app is running, it displays an overlay button (a green circle), initially at the center of the screen, but it can be moved around. Upon clicking the button, after several seconds the app displays a message with the information when the energy bar will be full. 

The message will disappear after a few seconds, but you can click on it to remove it immediately.

To remove the button, the app must be either closed from the list of the recent apps, or by long-pressing the button. (Though it tends to run away from the finger, I don't know what's up with it.)

## A sample screenshot:

![sample screenshot](docs/hm_screenshot_s.png)

## How it works:

It captures the screen, locates the energy bar in the upper right corner, and OCRs the text. It tries to take into account the countdown timer, but sometimes the OCR doesn't work with the timer, so the result may be incorrect by 4 minutes.

When run for the 1st time, it asks for the permission to capture the screen. The standard message says "will start capturing everything that's displayed on your screen". However, there's no need to worry about it, because the app only processes the upper right corner of the screen, and only for a few seconds after clicking the button. 

The SD card permissions (asked upon installing) are for saving screenshots - however, this option is currently disabled (see the sources where it's commented out). I don't want to remove it entirely because it's very useful for debugging.

It doesn't know which app is running on the background, so it will try to find the energy bar whenever you click the button, but it will just return an error - there's no sense in using it outside the Hogwarts Mystery game.

## Why:

I wrote it mostly as an Android programming exercise. I learned a lot of cool things, in particular, how to create overlay buttons, how to move them around (by processing onTouch events), how to capture the screen (using MediaProjection API), how to run a service and communicate with it, how to OCR the text (using TextRecognizer API). Maybe my code will help somebody else who's struggling with the same problems. Most likely it can be made more efficient, more elegant etc... but next time.


The launcher icon was adopted from the free Renewable Energy icon made by [Freepik](http://www.freepik.com) from [Flaticon](https://www.flaticon.com).

## Requirements:

Android version must be 5.0 (Lollipop) or higher, because it's the requirement for the MediaProjection API.

## Downloads:

The .apk can be [downloaded here](http://catness.org/hmenergycalculator).

