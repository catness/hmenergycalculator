package org.catness.hmenergycalculator;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {
    public static String LOG_TAG = Utils.class.getSimpleName();

    public static String str(int i) {
        return Integer.toString(i);
    }

    public static String str(float i) {
        return Float.toString(i);
    }

    public static String str(double i) {
        return Double.toString(i);
    }

    public static String str(boolean i) {
        return Boolean.toString(i);
    }

    public static Date addMinutesToNow(int minutes) {
        final long ONE_MINUTE_IN_MILLIS = 60000;//millisecs
        Date beforeTime = new Date();

        long curTimeInMs = beforeTime.getTime();
        Date afterAddingMins = new Date(curTimeInMs + (minutes * ONE_MINUTE_IN_MILLIS));
        return afterAddingMins;
    }

    public static String timeFromNow(int minutes) {
        if (minutes < 60) return Integer.toString(minutes) + "m";
        int h = minutes / 60;
        int m = minutes % 60;
        String output = Integer.toString(h) + "h";
        if (m != 0) {
            output += " " + Integer.toString(m) + "m";
        }
        return output;
    }

    private static String timeLeft(int mins) {
        Date date = addMinutesToNow(mins);
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        String strDate = dateFormat.format(date);
        String fromnow = timeFromNow(mins);
        return "Come back at " + strDate + "\n(" + fromnow + " from now)";
    }

    public static String calculateTime(NumberPair energy) {
        // when the time is not recognized
        int mins = (energy.max - energy.min) * 4;
        return timeLeft(mins);
    }

    public static String calculateTime(NumberPair energy, NumberPair time) {
        int mins_orig = (energy.max - energy.min) * 4; // number of minutes to wait (in the worst case)
        // time: max is seconds, min is minutes
        int diff = Math.round((time.min * 60 + time.max) / 60); // time till the next energy increment
        int mins = mins_orig - (4 - diff); // we have to wait a little less, because part of the time already passed

        Log.e(LOG_TAG, "mins_orig=" + str(mins_orig) + " max=" + str(time.max) + " min=" + str(time.min) + " diff=" + str(diff) + " result=" + mins);
        return timeLeft(mins);
    }

    private static NumberPair getTime(String minS, String maxS) {
        // process the countdown timer (example: 2:30)
        int min, max;
        NumberPair num = new NumberPair();
        try {
            min = Integer.parseInt(minS);
            max = Integer.parseInt(maxS);
        } catch (Exception e) {
            num.type = NumberPair.ERR_FORMAT;
            return num;
        }
        // minutes:seconds - minutes must be less than 4, seconds less than 60
        if (min < 0 || min > 3 || max < 0 || max > 59) {
            num.type = NumberPair.ERR_FORMAT;
            return num;
        }
        num.min = min;
        num.max = max;
        num.type = NumberPair.TIME;
        return num;
    }

    private static NumberPair getEnergy(String minS, String maxS) {
        // process the energy bar (example: 24/36)
        int min, max;
        NumberPair num = new NumberPair();
        try {
            min = Integer.parseInt(minS);
            max = Integer.parseInt(maxS);
        } catch (Exception e) {
            num.type = NumberPair.ERR_FORMAT;
            return num;
        }

        /* apparently it's possible for the min to be more than max, so this clause is not relevant
        if (min > max) {
            num.type = NumberPair.ERR_FORMAT;
            return num;
        }
        */

        if (min >= max) {
            num.type = NumberPair.ENERGY_FULL;
            return num;
        }
        num.min = min;
        num.max = max;
        num.type = NumberPair.ENERGY;
        return num;
    }

    public static NumberPair getNumbers(String input) {
        // find out if we have the countdown timer or the energy (or none of these)
        NumberPair num = new NumberPair();
        String[] output;
        if (input.equals("")) {
            num.type = NumberPair.ERR_EMPTY;
            return num;
        }
        if (input.indexOf(':') >= 0) {
            output = input.split(":");
            if (output.length != 2) {
                num.type = NumberPair.ERR_FORMAT;
                return num;
            } else {
                return getTime(output[0].trim(), output[1].trim());
            }
        }
        if (input.indexOf('/') >= 0) {
            output = input.split("/");
            if (output.length != 2) {
                num.type = NumberPair.ERR_FORMAT;
                return num;
            } else {
                return getEnergy(output[0].trim(), output[1].trim());
            }
        }
        num.type = NumberPair.ERR_FORMAT;
        return num;
    }

}
