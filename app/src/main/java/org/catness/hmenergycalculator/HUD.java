package org.catness.hmenergycalculator;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import java.lang.ref.WeakReference;

import static android.view.ViewConfiguration.getLongPressTimeout;
import static android.view.WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
import static android.view.WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;
import static android.view.WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION;
import static android.view.WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
import static java.lang.Math.abs;

public class HUD extends Service implements OnTouchListener {
    private static final String LOG_TAG = HUD.class.getSimpleName();
    public static boolean isRunning = false;
    ImageButton mButton;
    TextView mText;
    public static final String BROADCAST_ACTION = "org.catness.hmenergycalculator";
    private final Handler handler = new Handler();
    Intent myIntent;

    public static final int OP_SCREENSHOT = 1;
    public static final int OP_QUIT = 2;
    Context mycontext;
    public Messenger mMessenger = new Messenger(new MessageHandler(this));
    public static final String KEY_MESSAGE = "KEY_MESSAGE";
    WindowManager.LayoutParams params;

    private int x0, y0;
    private long lastTouchDown;
    private static final int CLICK_ACTION_THRESHHOLD = 200;
    private static final int LONGPRESS_ACTION_THRESHHOLD = getLongPressTimeout();
    private static final int LONGPRESS_COORDS_THRESHOLD = 10;
    private boolean longpressinit = false;

    private int displayWidth, displayHeight;
    private WindowManager wm;


    @Override
    public IBinder
    onBind(Intent intent) {
        return mMessenger.getBinder();
        // return null;
    }

    @Override
    public void onCreate() {
        // create the overlay button, and an invisible window for messages
        Log.e(LOG_TAG, "HUD: onCreate");
        super.onCreate();
        mButton = new ImageButton(this);
        mButton.setBackgroundResource(R.drawable.roundbutton);
        mButton.setOnTouchListener(this);

        mText = new TextView(this);
        mText.setBackgroundColor(Color.parseColor("#10ff10"));
        mText.setTextColor(Color.BLACK);
        mText.setPadding(40, 40, 40, 40);
        mText.setBackgroundResource(R.drawable.border);
        mText.setVisibility(View.INVISIBLE);

        params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                FLAG_NOT_TOUCH_MODAL | FLAG_NOT_FOCUSABLE | FLAG_TRANSLUCENT_STATUS | FLAG_TRANSLUCENT_NAVIGATION,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.CENTER;
        wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        wm.addView(mButton, params);

        params.gravity = Gravity.BOTTOM;
        wm.addView(mText, params);

        mycontext = getBaseContext();
        isRunning = true;
        myIntent = new Intent(BROADCAST_ACTION);
        params.gravity = Gravity.NO_GRAVITY;

        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);
        displayWidth = metrics.widthPixels;
        displayHeight = metrics.heightPixels;

        mText.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view.getVisibility() == View.VISIBLE) {
                    view.setVisibility(View.INVISIBLE);
                }
                return false;
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mButton != null) {
            ((WindowManager) getSystemService(WINDOW_SERVICE)).removeView(mButton);
            mButton = null;
        }

        if (mText != null) {
            ((WindowManager) getSystemService(WINDOW_SERVICE)).removeView(mText);
            mText = null;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        // this is called only for mButton, because mButton is the only one which has "this" as onTouchListener
        // GestureDetector is more convenient to detect long press and click
        // but it doesn't work together with processing motion events for moving the button (couldn't figure out how to do it)
        // so we have to detect the long press and click manually

        final int X = (int) event.getRawX();
        final int Y = (int) event.getRawY();

        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                x0 = X;
                y0 = Y;
                lastTouchDown = System.currentTimeMillis();
                longpressinit = true;
                break;
            case MotionEvent.ACTION_UP:
                long diff = System.currentTimeMillis() - lastTouchDown;
                if (diff < CLICK_ACTION_THRESHHOLD) {
                    // start the capture
                    myIntent.putExtra("op", OP_SCREENSHOT);
                    sendBroadcast(myIntent);

                    mText.setText("Please wait...");
                    mText.setVisibility(View.VISIBLE);

                } else if (diff > LONGPRESS_ACTION_THRESHHOLD) {
                    if (longpressinit) {
                        myIntent.putExtra("op", OP_QUIT);
                        sendBroadcast(myIntent);
                    }
                }
                longpressinit = false;
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE:
                if (longpressinit && (abs(x0 - X) > LONGPRESS_COORDS_THRESHOLD || abs(y0 - Y) > LONGPRESS_COORDS_THRESHOLD)) {
                    longpressinit = false;
                }
                // X and Y are relative from the initial position of the button, which is the middle of the display (because of gravity)
                params.x = X - displayWidth / 2;
                params.y = Y - displayHeight / 2;
                wm.updateViewLayout(mButton, params);
                break;
        }
        return true;
        // return gestureDetector.onTouchEvent(event); // this doesn't work
    }

    public class MessageHandler extends Handler {
        WeakReference<Context> contextReference;

        public MessageHandler(Context context) {
            contextReference = new WeakReference<>(context);
        }

        @Override
        public void handleMessage(Message msg) {
            if (mText == null) return;
            if (contextReference.get() != null) {
                String message = msg.getData().getString(KEY_MESSAGE);
                //Log.e(LOG_TAG, "got message! " + message);
                //Toast.makeText(contextReference.get(), message, Toast.LENGTH_SHORT).show();
                if (message.equals("OFF")) {
                    mText.setVisibility(View.INVISIBLE);
                } else {
                    mText.setText(message);
                    mText.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);
        displayWidth = metrics.widthPixels;
        displayHeight = metrics.heightPixels;
        //Log.e(LOG_TAG,"Orientation changed! displayWidth="+str(displayWidth) + " displayHeight="+displayHeight);
    }

}
