package org.catness.hmenergycalculator;

public class NumberPair {
    public static final int ENERGY_FULL = 0;
    public static final int ENERGY = 1;
    public static final int TIME = 2;

    public static final int ERR_EMPTY = 10;
    public static final int ERR_FORMAT = 11;

    public int min, max, type;

    public NumberPair() {
        type = ERR_EMPTY;
    }
}
