package org.catness.hmenergycalculator;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.util.Log;
import android.util.SparseArray;
import android.widget.Toast;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

public class OCR {
    private final String LOG_TAG = OCR.class.getSimpleName();
    private TextRecognizer textRecognizer;
    private Context context;

    public OCR(Context context) {
        this.context = context;
        textRecognizer = new TextRecognizer.Builder(context).build();

        if (!textRecognizer.isOperational()) {
            // Note: The first time that an app using a Vision API is installed on a
            // device, GMS will download a native libraries to the device in order to do detection.
            // Usually this completes before the app is run for the first time.  But if that
            // download has not yet completed, then the above call will not detect any text,
            // barcodes, or faces.
            // isOperational() can be used to check if the required native libraries are currently
            // available.  The detectors will automatically become operational once the library
            // downloads complete on device.
            Log.e(LOG_TAG, "Detector dependencies are not yet available.");

            // Check for low storage.  If there is low storage, the native library will not be
            // downloaded, so detection will not become operational.

            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = context.registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
                Toast.makeText(context, "Low Storage", Toast.LENGTH_LONG).show();
                Log.w(LOG_TAG, "Low Storage");
            }
        }
    }

    public String parse(Bitmap bitmap) {

        String result = "";
        Frame imageFrame = new Frame.Builder()
                .setBitmap(bitmap)
                .build();

        SparseArray<TextBlock> textBlocks = textRecognizer.detect(imageFrame);

        for (int i = 0; i < textBlocks.size(); i++) {
            TextBlock textBlock = textBlocks.get(textBlocks.keyAt(i));
            Log.e(LOG_TAG, "~~~~~" + textBlock.getValue());
            result += " " + textBlock.getValue();
        }

        result = result.trim().replace('O', '0');
        // the zero is represented with O glyph instead of 0 ! so parseInt throws an exception.
        // either it's the OCR's fault, or the devs use O because it looks better

        result = result.replaceAll("[A-Za-z]",""); // remove all alphabetic characters (OCRed by mistake from artefacts)
        return result;
    }
}
