package org.catness.hmenergycalculator;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.Image;
import android.media.ImageReader;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.OrientationEventListener;
import android.view.Window;
import android.view.WindowManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.Timer;
import java.util.TimerTask;

import static android.view.WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
import static android.view.WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE;
import static android.view.WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;
import static android.view.WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION;
import static android.view.WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
import static org.catness.hmenergycalculator.HUD.OP_QUIT;
import static org.catness.hmenergycalculator.HUD.OP_SCREENSHOT;
import static org.catness.hmenergycalculator.Utils.calculateTime;
import static org.catness.hmenergycalculator.Utils.getNumbers;
import static org.catness.hmenergycalculator.Utils.str;

public class TaskBarView extends Activity {
    private static final String LOG_TAG = TaskBarView.class.getSimpleName();

    private static final int REQUEST_CODE = 100;
    private static String STORE_DIRECTORY;
    private static int IMAGES_PRODUCED;
    private static final String SCREENCAP_NAME = "screencap";
    private static final int VIRTUAL_DISPLAY_FLAGS = DisplayManager.VIRTUAL_DISPLAY_FLAG_OWN_CONTENT_ONLY | DisplayManager.VIRTUAL_DISPLAY_FLAG_PUBLIC;
    private static MediaProjection sMediaProjection;

    private MediaProjectionManager mProjectionManager;
    private ImageReader mImageReader;
    private Handler mHandler;
    private Display mDisplay;
    private VirtualDisplay mVirtualDisplay;
    private int mDensity;
    private int mWidth;
    private int mHeight;
    private int mRotation;
    private OrientationChangeCallback mOrientationChangeCallback;
    private Messenger mMessenger;
    private boolean isBound = false;
    private static boolean finish = false;
    private Connection mServiceConn;

    private boolean SCREENSHOT = false;   // set to true to enable screenshots for debugging
    private static Intent screenshotPermission = null;
    private OCR ocr;
    private NumberPair time, energy;
    private String energy_input;
    private long startTime, prevTime;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //Log.e(LOG_TAG, "On Create!");
        // NOTE: using Log.e everywhere, because Android Studio, when connected to my phone (Huawei Mate 10 Lite), refuses to display any other loglevels

        // make the activity window invisible
        // as far as I remember, the manifest settings are not enough
        Window window = getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        window.addFlags(FLAG_TRANSLUCENT_STATUS | FLAG_TRANSLUCENT_NAVIGATION | FLAG_NOT_FOCUSABLE | FLAG_NOT_TOUCHABLE | FLAG_NOT_TOUCH_MODAL);
        window.setAttributes(params);
        setVisible(false);

        super.onCreate(savedInstanceState);
        prevTime = System.currentTimeMillis();
        // call for the projection manager
        mProjectionManager = (MediaProjectionManager) getSystemService(Context.MEDIA_PROJECTION_SERVICE);

        // start the service with the overlay button
        Intent svc = new Intent(this, HUD.class);
        startService(svc);
        mServiceConn = new Connection(this);
        bindService(new Intent(this, HUD.class), mServiceConn, BIND_AUTO_CREATE);

        ocr = new OCR(getApplicationContext());  // Google OCR API

        time = new NumberPair();  // countdown timer on the energy bar
        energy = new NumberPair();  // energy numbers on the energy bar
        // for media projection, to call it continuously
        new Thread() {
            @Override
            public void run() {
                Looper.prepare();
                mHandler = new Handler();
                Looper.loop();
            }
        }.start();
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // communication from the service - either to start screenshots, or to quit
            //Log.e(LOG_TAG, "onReceive");
            int op = intent.getIntExtra("op", 0);
            switch (op) {
                case OP_SCREENSHOT:
                    startProjection();
                    //moveTaskToBack(true);
                    break;
                case OP_QUIT:
                    Log.e(LOG_TAG, "quitting...");
                    Intent svc = new Intent(getBaseContext(), HUD.class);
                    stopService(svc);
                    finishAndRemoveTask();
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void onStart() {
        //Log.e(LOG_TAG, "On start!");
        super.onStart();
    }

    @Override
    public void onResume() {
        //Log.e(LOG_TAG, "On resume!");
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter(HUD.BROADCAST_ACTION));
    }

    @Override
    public void onDestroy() {
        //Log.e(LOG_TAG, "On destroy!");
        super.onDestroy();
        Intent svc = new Intent(getBaseContext(), HUD.class);
        stopService(svc);
        unregisterReceiver(broadcastReceiver);
        unbindService(mServiceConn);
    }

    // image capturing stuff adapted from https://github.com/mtsahakis/MediaProjectionDemo

    private class ImageAvailableListener implements ImageReader.OnImageAvailableListener {
        @Override
        public void onImageAvailable(ImageReader reader) {
            Image image = null;
            FileOutputStream fos = null;
            Bitmap bitmap_orig = null;
            Bitmap bitmap = null;

            if (finish) return;  // ocr already finished, no need to acquire another image

            try {
                image = reader.acquireLatestImage();

                // the energy bar stays up for 3 sec, and the timer for another 3 sec
                // so we wait for (currently 0.7 sec) - give some time to the numbers to change.
                // but we have to acquire the image anyway, even if it's not needed, otherwise the program gets stuck
                long currentTime = System.currentTimeMillis();
                long diff = currentTime - prevTime;
                if (startTime==0) startTime = currentTime; // I don't know why startTime is 0 sometimes when the button is clicked the 1st time after recompiling
                long elapsed = (currentTime - startTime) / 1000; // elapsed seconds
               // Log.e(LOG_TAG,"time="+currentTime + "  startTime=" + startTime + " prevTime=" + prevTime + " diff=" + diff+ " elapsed="+elapsed);
                if (diff < 700) return; // less than 0.7 sec from the previous call
                prevTime = currentTime;

                if (image != null) {
                    Image.Plane[] planes = image.getPlanes();
                    ByteBuffer buffer = planes[0].getBuffer();
                    int pixelStride = planes[0].getPixelStride();
                    int rowStride = planes[0].getRowStride();
                    int rowPadding = rowStride - pixelStride * mWidth;

                    // create bitmap
                    bitmap_orig = Bitmap.createBitmap(mWidth + rowPadding / pixelStride, mHeight, Bitmap.Config.ARGB_8888);
                    bitmap_orig.copyPixelsFromBuffer(buffer);

                    // select a rectangle in the upper right corner (where the energy bar is)
                    int width_orig = mWidth + rowPadding / pixelStride;
                    int height_orig = mHeight;
                    int width = width_orig / 5;
                    int x = width_orig - width;
                    int height = height_orig / 10;
                    int y = 0;
                    bitmap = Bitmap.createBitmap(bitmap_orig, x, y, width, height);

                    int[] pixels = new int[width * height];
                    //get pixels
                    bitmap.getPixels(pixels, 0, width, 0, 0, width, height);

                    // preprocess the bitmap - remove the cyan highlighted pixels (make them dark), otherwise OCR doesn't work
                    for (int i = 0; i < pixels.length; ++i) {
                        int pixel = pixels[i];
                        int r = (pixel & 0x00ff0000) >> 16;
                        int g = (pixel & 0x0000ff00) >> 8;
                        int b = (pixel & 0x000000ff);

                        if (
                             (r < 0xe0 && g >= 0xa0 && b >= 0xa0) ||
                             (r < 0xc0 && g < 0xc0 && b < 0xc0)) {
                            pixels[i] = 0xff132934;
                        }
                    }
                    // create result bitmap output
                    Bitmap processed = Bitmap.createBitmap(width, height, bitmap.getConfig());
                    //set pixels
                    processed.setPixels(pixels, 0, width, 0, 0, width, height);


                    // write bitmap to a file
                    // /storage/emulated/0/Android/data/org.catness.hmenergycalculator/files/screenshots//myscreen_NUM.png
                    // it may be not allowed to see the listing of /storage/emulated in a file manager
                    // so, go directly to /storage/emulated/0/Android/data/
                    if (SCREENSHOT) {
                        String filename = STORE_DIRECTORY + "/myscreen_" + IMAGES_PRODUCED + ".png";
                        fos = new FileOutputStream(filename);
                        Log.e(LOG_TAG, "saving file: " + filename);
                        processed.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                    }

                    ++IMAGES_PRODUCED;
                    String input = ocr.parse(processed);
                    NumberPair num = getNumbers(input);
                    Log.e(LOG_TAG, " retries: " + str(IMAGES_PRODUCED));
                    // check if we got the timer or the energy (or nothing)
                    // optimally, we need both, but sometimes we get only the energy, because OCR doesn't work well for the timer
                    switch (num.type) {
                        case NumberPair.ENERGY:
                            Log.e(LOG_TAG, "Energy: " + str(num.min) + " / " + str(num.max));
                            energy = num;
                            energy_input = input;
                            if (time.type == NumberPair.TIME) { // we already have the timer, now got the energy
                                finish = true;
                                String message = calculateTime(energy, time);
                                sendMessage(input + "\n" + message);
                            }
                            break;
                        case NumberPair.ENERGY_FULL:
                            Log.e(LOG_TAG, "Energy full");
                            finish = true;  // energy full - no need for the timer (it's not displayed anyway)
                            sendMessage(input + "\n" + "Energy is full!");
                            break;
                        case NumberPair.TIME:
                            Log.e(LOG_TAG, "Time: " + str(num.min) + " : " + str(num.max));
                            time = num;
                            if (energy.type == NumberPair.ENERGY) {  // we already have the energy, now got the timer
                                finish = true;
                                String message = calculateTime(energy, time);
                                sendMessage(energy_input + "\n" + message);
                            }
                            break;
                        default:
                            Log.e(LOG_TAG, "Input not recognized : " + input);
                            break;
                    }

                    if (!finish && (elapsed > 6)) { // 6 seconds passed, and we still haven't got both the timer and the energy - have to give up
                        finish = true;
                        if (energy.type == NumberPair.ENERGY) {
                            // we have the energy but no timer - good enough for an approx. estimation
                            String message = calculateTime(energy);
                            sendMessage(energy_input + "\n" + message);
                        } else {
                            sendMessage("Energy bar not found");
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                // cleanup
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                }

                if (bitmap != null) {
                    bitmap.recycle();
                }
                if (bitmap_orig != null) {
                    bitmap_orig.recycle();
                }

                if (image != null) {
                    image.close();
                }
                if (finish) {
                    stopProjection();
                }
                moveTaskToBack(true); // release the focus so the invisible window of the main activity doesn't freeze the app we're capturing
                // this has to be done rather sooner than later, but it can't be called immediately - I don't remember why, but it doesn't work
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Log.e(LOG_TAG, "Pause!");
    }

    private void sendMessage(String mymessage) {
        // send the message to the service, to be displayed
        if (isBound) {
            Message message = Message.obtain();
            Bundle bundle = new Bundle();
            bundle.putString(HUD.KEY_MESSAGE, mymessage);
            //Log.e(LOG_TAG, "sending message to the service : tries=" + str(IMAGES_PRODUCED));
            message.setData(bundle);

            try {
                mMessenger.send(message);
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            // hide the message window in the service after "delay" seconds
            int delay = 6;
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    // this code will be executed after "delay" seconds
                    Message message = Message.obtain();
                    Bundle bundle = new Bundle();
                    bundle.putString(HUD.KEY_MESSAGE, "OFF");
                    message.setData(bundle);

                    try {
                        mMessenger.send(message);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                }
            }, delay * 1000);
        }
    }

    private class OrientationChangeCallback extends OrientationEventListener {

        OrientationChangeCallback(Context context) {
            super(context);
        }

        @Override
        public void onOrientationChanged(int orientation) {
            final int rotation = mDisplay.getRotation();
            if (rotation != mRotation) {
                mRotation = rotation;
                try {
                    // clean up
                    if (mVirtualDisplay != null) {
                        mVirtualDisplay.release();
                        mVirtualDisplay = null;
                    }
                    if (mImageReader != null) {
                        mImageReader.setOnImageAvailableListener(null, null);
                        mImageReader = null;
                    }
                    // re-create virtual display depending on device width / height
                    createVirtualDisplay();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class MediaProjectionStopCallback extends MediaProjection.Callback {
        @Override
        public void onStop() {
            Log.e("ScreenCapture", "stopping projection.");
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (mVirtualDisplay != null) mVirtualDisplay.release();
                    if (mImageReader != null) mImageReader.setOnImageAvailableListener(null, null);
                    if (mOrientationChangeCallback != null) mOrientationChangeCallback.disable();
                    sMediaProjection.unregisterCallback(MediaProjectionStopCallback.this);
                }
            });
        }
    }

    private void createVirtualDisplay() {
        Point size = new Point();
        mDisplay.getSize(size);
        mWidth = size.x;
        mHeight = size.y;

        // start capture reader
        try {
            mImageReader = ImageReader.newInstance(mWidth, mHeight, PixelFormat.RGBA_8888, 2);
            mVirtualDisplay = sMediaProjection.createVirtualDisplay(SCREENCAP_NAME, mWidth, mHeight, mDensity, VIRTUAL_DISPLAY_FLAGS, mImageReader.getSurface(), null, mHandler);
            mImageReader.setOnImageAvailableListener(new ImageAvailableListener(), mHandler);
        } catch (Exception e) {
            Log.e(LOG_TAG, "exception in createVirtualDisplay: " + e.getMessage());
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            super.onActivityResult(requestCode, resultCode, data);

            if (Activity.RESULT_OK == resultCode) {
                //setScreenshotPermission((Intent) data.clone()); //I don't remember what is that for?
            } else if (Activity.RESULT_CANCELED == resultCode) {
                //setScreenshotPermission(null);
                Log.e(LOG_TAG, "screenshot access denied");
                sendMessage("Screenshot access denied");
                return;
            }

            sMediaProjection = mProjectionManager.getMediaProjection(resultCode, data);

            if (sMediaProjection != null) {

                if (SCREENSHOT) {
                    File externalFilesDir = getExternalFilesDir(null);
                    if (externalFilesDir != null) {
                        STORE_DIRECTORY = externalFilesDir.getAbsolutePath() + "/screenshots";
                        File storeDirectory = new File(STORE_DIRECTORY);
                        if (!storeDirectory.exists()) {
                            boolean success = storeDirectory.mkdirs();
                            if (!success) {
                                Log.e(LOG_TAG, "failed to create file storage directory.");
                                return;
                            }
                        }
                    } else {
                        Log.e(LOG_TAG, "failed to create file storage directory, getExternalFilesDir is null.");
                        return;
                    }
                }

                // display metrics
                DisplayMetrics metrics = getResources().getDisplayMetrics();
                mDensity = metrics.densityDpi;
                mDisplay = getWindowManager().getDefaultDisplay();

                // create virtual display depending on device width / height
                createVirtualDisplay();

                // register orientation change callback
                mOrientationChangeCallback = new OrientationChangeCallback(this);
                if (mOrientationChangeCallback.canDetectOrientation()) {
                    mOrientationChangeCallback.enable();
                }

                // register media projection stop callback
                sMediaProjection.registerCallback(new MediaProjectionStopCallback(), mHandler);
            }
        }
    }

    public void startProjection() {
        IMAGES_PRODUCED = 0;
        finish = false;
        time.type = NumberPair.ERR_EMPTY;
        energy.type = NumberPair.ERR_EMPTY;
        startTime = System.currentTimeMillis();
        startActivityForResult(mProjectionManager.createScreenCaptureIntent(), REQUEST_CODE);
    }

    private void stopProjection() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (sMediaProjection != null) {
                    sMediaProjection.stop();
                }
                moveTaskToBack(true);
            }
        });
    }

    public static class Connection implements ServiceConnection {
        // to send messages to the service - to display the OCR results
        WeakReference<Context> contextReference;

        public Connection(Context context) {
            contextReference = new WeakReference<>(context);
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Context context = contextReference.get();
            if (context != null) {
                ((TaskBarView) context).mMessenger = new Messenger(service);
                ((TaskBarView) context).isBound = true;
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Context context = contextReference.get();
            if (context != null) {
                ((TaskBarView) context).isBound = false;
            }
        }
    }
}


